import 'package:flutter/material.dart';
import 'dart:async';
import 'package:drinkx/screens/settings.dart';
import 'package:drinkx/screens/addDrink.dart';
import 'package:drinkx/screens/intro.dart';
import 'package:drinkx/model/model.dart';
import 'package:drinkx/database/database.dart';

void main() => runApp(new MyApp());

String currentAmount = "0";

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Drinkz',
      theme: new ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: new MyHomePage(),
      routes: <String, WidgetBuilder> {
        '/home': (BuildContext context) => new MyHomePage(),
        '/intro': (BuildContext context) => new IntroScreen()
      },
    );
  }
}

Future<List<Info>> fetchInfo() async {
  var dbHelper = DBHelper();
  Future<List<Info>> infos = dbHelper.getInfo();
  return infos;
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {  
  String goal;
  String weight;
  String gender;

  void reset() {
    String now = DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString();

    currentAmount = "0";
    var info = Info(goal, currentAmount, weight, gender, now);
    var dbHelper = DBHelper();
    dbHelper.updateInfo(info);
    refresh();
  }

  void refresh() {
    fetchInfo().then((val) => setState(() {  
      if (val.toString() != "[]") {
        String lastModified = val[0].lastModified;
        String now = DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString();

        gender = val[0].gender;
        weight = val[0].weight;
        goal = val[0].goal;

        if (now != lastModified) {
          reset();
        }

        if (double.tryParse(val[0].currentAmount) >= double.tryParse(val[0].goal)) {
          currentAmount = "100";
        } else {
          currentAmount = (double.tryParse(val[0].currentAmount) / (double.tryParse(val[0].goal)) * 100).toStringAsFixed(0);
        }
      } else {
        currentAmount = "0";
      }
    }));
  } 

  _MyHomePageState() {
    refresh();
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      refresh();
    }

  @override
  Widget build(BuildContext context) {    
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.invert_colors),
        title: Text("Drinkz"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            tooltip: 'Settings',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SettingsPage()), 
              );
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              alignment: AlignmentDirectional(0.0, 0.0),
              children: <Widget>[
                AnimatedContainer(
                    color: Colors.cyan,
                    duration: Duration(seconds: 1),
                    curve: Curves.decelerate,
                    width: MediaQuery.of(context).size.width,
                    height: ((MediaQuery.of(context).size.height - 80) * double.tryParse(currentAmount)) / 100,
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Today you drank:',
                      ),
                      Text(
                        currentAmount + "%", style: Theme.of(context).textTheme.display1,
                      ),
                      FutureBuilder<List<Info>>(
                        future: fetchInfo(),
                        builder: (context, snapshot) {
                          if (snapshot.data.toString() == "[]") {
                            Future.delayed(Duration(milliseconds: 500)).then((_) {
                              Navigator.of(context).pushReplacementNamed('/intro');
                            });
                          }

                          return new Container();
                        }
                      ),
                    ]
                  )
                ),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddDrinkPage()),
          ).then((value) {
            refresh();
          });
          //_execute();
        },
        tooltip: 'Add a drink',
        child: Icon(Icons.add),
      ),
    );
  }
}