import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:drinkx/model/model.dart';

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }

    _db = await initDb();
    return _db;
  }

  initDb() async {
    Directory documentsDir = await getApplicationDocumentsDirectory();
    String path = join(documentsDir.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE Info(id INTEGER PRIMARY KEY, goal TEXT, currentAmount TEXT, weight TEXT, gender TEXT, lastModified TEXT)");
    print("Created table");
  }

  Future<List<Info>> getInfo() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM Info');
    List<Info> infos = new List();
    
    for (int i = 0; i < list.length; i++) {
      infos.add(new Info(list[i]["goal"], list[i]["currentAmount"], list[i]["weight"], list[i]["gender"], list[i]["lastModified"]));
    }

    return infos;
  }

  void saveInfo(Info info) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawInsert("INSERT INTO Info(goal, currentAmount, weight, gender, lastModified) VALUES('" + info.goal + "', '" + info.currentAmount + "', '" + info.weight + "', '" + info.gender + "', '" + info.lastModified + "')");
    });
  }

  void updateInfo(Info info) async {
    var dbClient = await db;
    await dbClient.transaction((txn) async {
      return await txn.rawUpdate("UPDATE Info SET goal = '${info.goal}', currentAmount = '${info.currentAmount}', weight = '${info.weight}', gender = '${info.gender}', lastModified = '${info.lastModified}' WHERE id = 1;");
    });
  }
}