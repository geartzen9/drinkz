import 'package:flutter/material.dart';
import 'package:drinkx/database/database.dart';
import 'package:drinkx/model/model.dart';

class AddDrinkPage extends StatefulWidget {
  AddDrinkPage({Key key}) : super(key: key);

  @override
  _AddDrinkState createState() => new _AddDrinkState();
}

class _AddDrinkState extends State<AddDrinkPage> {
  String currentAmount;
  String goal;
  String weight;
  String gender;

  final amountController = TextEditingController();

  _AddDrinkState() {
    fetchInfo().then((val) => setState(() {
      currentAmount = val[0].currentAmount;
      goal = val[0].goal;
      weight = val[0].weight;
      gender = val[0].gender;
    }));
  }

  Future<List<Info>> fetchInfo() async {
    var dbHelper = DBHelper();
    Future<List<Info>> infos = dbHelper.getInfo();
    return infos;
  }

  void calculateNewAmount(String added, double percentage) {
    String now = DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString();
    String addedCalculated = (double.tryParse(added) / 100.00 * percentage).toString();
    
    currentAmount = (double.tryParse(currentAmount) + double.tryParse(addedCalculated)).toString();
    var info = Info(goal, currentAmount, weight, gender, now);
    var dbHelper = DBHelper();
    dbHelper.updateInfo(info);
  }

  final drinkzIcons = [AssetImage('assets/water@3x.png'), AssetImage('assets/tea@3x.png'), AssetImage('assets/coffee@3x.png'), AssetImage('assets/juice@3x.png'), AssetImage('assets/sports@3x.png'), AssetImage('assets/energy@3x.png'), AssetImage('assets/milk@3x.png'), AssetImage('assets/soda@3x.png'), AssetImage('assets/beer@3x.png'), AssetImage('assets/wine@3x.png'), AssetImage('assets/liquor@3x.png')];
  final drinkzTexts = ["Water", "Tea", "Coffee", "Juice", "Sports Drink", "Energy Drink", "Milk", "Soda", "Beer", "Wine", "Liquor"];
  final drinkzIntake = [100.00, 100.0, 90.0, 100.0, 120.0, 100.0, 120.0, 100.0, 90.0, 80.0, 50.0];

  Future<int> askAmount(BuildContext context, int index) {
    return showDialog<int>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text("Amount (ml)"),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0),
              child: Center(
                child: Wrap(
                  children: <Widget>[
                    TextField(
                      controller: amountController,
                      keyboardType: TextInputType.number,
                    ),

                    Builder(
                      builder: (context) =>
                      FlatButton( 
                        child: Text("OK"),
                        onPressed: () {
                          calculateNewAmount(amountController.text, 100.00);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        }
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("CANCEL"),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add a drink"),
      ),
      body: new GridView.count(
        crossAxisCount: 3,
        children: List.generate(drinkzTexts.length, (index) {
          return GridTile(
            child: FlatButton(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: drinkzIcons[index],
                      height: 75.0,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text(drinkzTexts[index])
                    )
                  ],
                ),
              ),
              onPressed: () { askAmount(context, index); },
            ),
          );
        }),
      ),
    );
  }
}