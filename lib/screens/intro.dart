import 'package:flutter/material.dart';
import 'package:drinkx/model/model.dart';
import 'package:drinkx/database/database.dart';

class IntroScreen extends StatefulWidget {
  final String title;

  IntroScreen({Key key, this.title}) : super(key: key);

  @override
  _IntroState createState() => new _IntroState();
}

class _IntroState extends State<IntroScreen> {
  int groupValue = 1;
  String goal;
  String currentAmount = "0";
  String weight;
  String gender;
  
  void submit() {
    if (groupValue == 1) {
      gender = "Male";
      goal = (33.33 * double.tryParse(weight)).toStringAsFixed(0);
    } else if (groupValue == 2) {
      gender = "Female";
      goal = (26.67 * double.tryParse(weight)).toStringAsFixed(0);
    } else {
      gender = "Other";
      goal = (30.00 * double.tryParse(weight)).toStringAsFixed(0);
    }

    String now = DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString();

    var info = Info(goal, currentAmount, weight, gender, now);
    var dbHelper = new DBHelper();
    dbHelper.saveInfo(info);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width - 60.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.invert_colors,
                    size: 60.0,
                  ),
                  Text(
                    "Welcome to Drinkz!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),
                  ),
                  Text(
                    "We first need to setup a few things."
                  ),
                ]
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 60.0,
            child: Wrap(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "What is your weight in kilos?",
                      labelStyle: TextStyle(color: Colors.black),
                      hintStyle: TextStyle(color: Colors.cyan)
                    ),
                    onChanged: (String value) {
                      weight = value;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text("What is your gender?"),
                ),
                Row(
                  children: <Widget>[
                    Radio(
                      value: 1,
                      onChanged: (int e) => changeGender(e),
                      activeColor: Colors.lightBlue,
                      groupValue: groupValue,
                    ),
                    Text("Male"),
                    Radio(
                      value: 2,
                      onChanged: (int e) => changeGender(e),
                      activeColor: Colors.lightBlue,
                      groupValue: groupValue,
                    ),
                    Text("Female"),
                    Radio(
                      value: 3,
                      onChanged: (int e) => changeGender(e),
                      activeColor: Colors.lightBlue,
                      groupValue: groupValue,
                    ),
                    Text("Other"),
                  ],
                ),
              ],
            ),
          ),
          Center(
            child: Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: RaisedButton(
                      child: Text("CONTINUE"),
                      color: Colors.cyan,
                      onPressed: () {
                        submit();
                        Navigator.of(context).pushReplacementNamed("/home");
                      },
                    )
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void changeGender(int e) {
    setState(() {
      switch(e) {
        case 1:
          groupValue = 1;
          break;
        case 2:
          groupValue = 2;
          break;
        case 3:
          groupValue = 3;
          break;
      }   
    });
  }
}