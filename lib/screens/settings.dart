import 'package:flutter/material.dart';
import 'package:drinkx/database/database.dart';
import 'package:drinkx/model/model.dart';
import 'dart:async';

Future<List<Info>> fetchInfo() async {
  var dbHelper = DBHelper();
  Future<List<Info>> infos = dbHelper.getInfo();
  return infos;
}

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsState createState() => new _SettingsState();
}

class _SettingsState extends State<SettingsPage> {
  int groupValue;

  String goal;
  String currentAmount;
  String weight;
  String gender;
  TextEditingController weightController = new TextEditingController();

  _SettingsState() {
    fetchInfo().then((val) => setState(() {
      weight = val[0].weight;
      weightController.text = weight;

      switch(val[0].gender) {
        case "Male":
          groupValue = 1;
          break;
        case "Female":
          groupValue = 2;
          break;
        case "Other":
          groupValue = 3;
          break;
      }
    }));
  }

  void _submitSettings() {
    switch(groupValue) {
      case 1:
        gender = "Male";
        goal = (33.33 * double.tryParse(weight)).toString();
        break;
      case 2:
        gender = "Female";
        goal = (26.67 * double.tryParse(weight)).toString();
        break;
      case 3:
        gender = "Other";
        goal = (30 * double.tryParse(weight)).toString();
        break;
    }

    String now = DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString();

    var info = Info(goal, currentAmount, weight, gender, now);
    var dbHelper = DBHelper();
    dbHelper.updateInfo(info);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(padding: new EdgeInsets.only(top: 20.0)),
            Container(
              width: MediaQuery.of(context).size.width - 30.0,
              child: Wrap(
                direction: Axis.horizontal,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(top: 20.0)),
                  TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "Weight (kg):",
                    ),
                    onChanged: (String value) {
                      weight = value;
                    },
                    controller: weightController,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0, right: 10.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 30.0,
                      child: Text("Gender:"),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 30.0,
                    child: Wrap(
                      children: <Widget>[
                        Radio(
                          value: 1,
                          onChanged: (int e) => changeGender(e),
                          activeColor: Colors.cyan,
                          groupValue: groupValue,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15.0),
                          child: Text("Male"),
                        ),
                        Radio(
                          value: 2,
                          onChanged: (int e) => changeGender(e),
                          activeColor: Colors.cyan,
                          groupValue: groupValue,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15.0),
                          child: Text("Female"),
                        ),
                        Radio(
                          value: 3,
                          onChanged: (int e) => changeGender(e),
                          activeColor: Colors.lightBlue,
                          groupValue: groupValue,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15.0),
                          child: Text("Other"),
                        ),
                      ]
                    ),
                  ),
                  Builder(
                    builder: (context) =>
                    RaisedButton( 
                      child: Text("Save"),
                      onPressed: () {
                        _submitSettings();
                        Scaffold.of(context).showSnackBar(new SnackBar(content: new Text("New settings saved!")));
                      }
                    )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changeGender(int e) {
    setState(() {
      switch(e) {
        case 1:
          groupValue = 1;
          break;
        case 2:
          groupValue = 2;
          break;
        case 3:
          groupValue = 3;
          break;
      }   
    });
  }
}