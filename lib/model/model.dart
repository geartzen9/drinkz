class Info {
  String goal;
  String currentAmount;
  String weight;
  String gender;
  String lastModified;

  Info(this.goal, this.currentAmount, this.weight, this.gender, this.lastModified);
}